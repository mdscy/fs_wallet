<?php include 'includes/header.php'; ?>

            <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col block">
                        <div class="mdl-tabs mdl-js-tabs">
                        <div class="mdl-tabs__tab-bar">
                            <a href="#tab1" class="mdl-tabs__tab is-active"><i class="fas fa-exchange-alt"></i> Exchange</a>
                        </div>
                        <div class="mdl-tabs__panel is-active" id="tab1">
                            
                            <form action="#">
                                <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--12-col">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                        <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                        <input type="hidden" value="" name="type">
                                        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                        <label for="type" class="mdl-textfield__label">From My Card With ID</label>
                                        <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                            <li class="mdl-menu__item" data-val="GBP">8762 14** **** 6020</li>
                                            <li class="mdl-menu__item" data-val="USD">8762 14** **** 6021</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--6-col">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                            <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                            <input type="hidden" value="" name="type">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            <label for="fromcurrency" class="mdl-textfield__label">From Currency</label>
                                            <ul for="fromcurrency" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <li class="mdl-menu__item" data-val="EUR">EUR</li>
                                                <li class="mdl-menu__item" data-val="GBP">GBP</li>
                                                <li class="mdl-menu__item" data-val="USD">USD</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mdl-cell mdl-cell--6-col">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                            <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                            <input type="hidden" value="" name="type">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            <label for="tocurrency" class="mdl-textfield__label">To Currency</label>
                                            <ul for="tocurrency" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <li class="mdl-menu__item" data-val="EUR">EUR</li>
                                                <li class="mdl-menu__item" data-val="GBP">GBP</li>
                                                <li class="mdl-menu__item" data-val="USD">USD</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mdl-cell mdl-cell--6-col">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                            <input class="mdl-textfield__input" type="text" id="amount">
                                            <label class="mdl-textfield__label" for="amount">Amount to Exchange</label>
                                        </div>
                                    </div>
                                    <div class="mdl-cell mdl-cell--6-col">
                                    <h4>100 EUR = 89.8800GBP</h4>
                                    <p>1 EUR = 0.898800 GBP</p>
                                    </div>
                                    <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore float-right">
                                </div>
                            </form>

                        </div>
                        </div>
                    </div>

                </div>
            </div>
<?php include 'includes/footer.php'; ?>
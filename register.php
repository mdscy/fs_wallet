<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/material.min.css">
        <link rel="stylesheet" href="css/style.css?v=02052018357">
        <script src="js/material.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
        <title>Register</title>
    </head>
    <body class="login-register">
    
    <div class="loginbox">
        <div class="wrapper--login">
            <div class="logo"><img src="img/fs--logo.png"></div>
            <h5>Personal Account Registration</h5>
            <form action="#">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="firstname">
                <label class="mdl-textfield__label" for="firstname">First Name</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="lastname">
                <label class="mdl-textfield__label" for="lastname">Last Name</label>
            </div>
           <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                 <input class="mdl-textfield__input" type="date" id="startdate" value="<?php echo date("Y-m-d");?>">
                <label class="mdl-textfield__label" for="startdate">Birthday</label>
            </div>
            <div class="choices">
                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                <input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1">
                <span class="mdl-radio__label">Female</span>
                </label>
                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="2">
                <span class="mdl-radio__label">Male</span>
                </label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select countrycode">
                <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                <input type="hidden" value="" name="type">
                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                <label for="type" class="mdl-textfield__label">Code</label>
                <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                    <li class="mdl-menu__item" data-val="EUR">+30</li>
                </ul>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label phonenumber">
                <input class="mdl-textfield__input" type="number" id="phonenumber">
                <label class="mdl-textfield__label" for="phonenumber">Phone Number</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="addressline1">
                <label class="mdl-textfield__label" for="addressline1">Address Line 1</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="addressline2">
                <label class="mdl-textfield__label" for="addressline2">Address Line 2</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="postcode">
                <label class="mdl-textfield__label" for="postcode">Post Code</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city">
                <label class="mdl-textfield__label" for="city">City</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                <input type="text" value="" class="mdl-textfield__input" id="country">
                <input type="hidden" value="" name="country">
                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                <label for="country" class="mdl-textfield__label">Country</label>
                <ul for="country" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                    <li class="mdl-menu__item" data-val="Cyprus">Cyprus</li>
                    <li class="mdl-menu__item" data-val="Greece">Greece</li>
                </ul>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="sample3">
                <label class="mdl-textfield__label" for="sample3">Email Address</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="password" id="password">
                <label class="mdl-textfield__label" for="password">Password</label>
            </div>
            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
            <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
            <span class="mdl-checkbox__label">I Aggree with the Terms and Conditions.</span>
            </label>
            <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore button-white" data-upgraded=",MaterialButton">
            </form>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/app.js?v=02052018357"></script>
    </body>
</html>
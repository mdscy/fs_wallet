<?php include 'includes/header.php'; ?>

            <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col block">
                        <div class="mdl-tabs mdl-js-tabs">
                        <div class="mdl-tabs__tab-bar">
                            <a href="#tab1" class="mdl-tabs__tab is-active"><i class="fas fa-university"></i> SEPA Incoming</a>
                            <a href="#tab2" class="mdl-tabs__tab"><i class="fas fa-credit-card"></i> From My Card</a>
                        </div>
                        <div class="mdl-tabs__panel is-active" id="tab1">

                            <form action="#">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="fullname" value="Name Surname" readonly>
                                <label class="mdl-textfield__label" for="fullname">Beneficiary</label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="iban" value="IE57PFSR99107000661733" readonly>
                                <label class="mdl-textfield__label" for="iban">IBAN Number</label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="swift" value="PFSRIE21" readonly>
                                <label class="mdl-textfield__label" for="swift">SWIFT / BIC</label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="walletid" value="400000661733" readonly>
                                <label class="mdl-textfield__label" for="walletid">Wallet ID</label>
                            </div>
                                <div class="mdl-components__warning">
                                    <p><b>Important Note:</b></p>
                                    <p>1. Supported countries : Austria, Germany, Netherlands, Belgium, Greece, Poland, Bulgaria, Hungary, Portugal, Cyprus, Ireland, Romania, Czech Republic, Italy, Slovak Republic, Denmark, Latvia, Slovenia, Estonia, Lithuania, Spain, Finland, Luxembourg, Sweden, France, Malta, United Kingdom, Croatia, Iceland , Monaco , Switzerland , Liechtenstein , Norway, San Marino.</p>
                                    <p>2. Make sure your bank uses the above BIC code.</p>
                                    <p>3. It can take up to 2 days for transfers to arrive.</p>
                                </div>
                            </form>

                        </div>
                        <div class="mdl-tabs__panel" id="tab2">
                            
                            <form action="#">
                            <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--3-col">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="Currency" value="EUR" readonly>
                                        <label class="mdl-textfield__label" for="Currency">Currency</label>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--6-col">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                        <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                        <input type="hidden" value="" name="type">
                                        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                        <label for="type" class="mdl-textfield__label">From Card</label>
                                        <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                            <li class="mdl-menu__item" data-val="GBP">8762 14** **** 6020</li>
                                            <li class="mdl-menu__item" data-val="USD">8762 14** **** 6021</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--3-col">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="amount">
                                        <label class="mdl-textfield__label" for="amount">Amount to Deposit</label>
                                    </div>
                                </div>
                                <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore float-right">
                            </div>
                            </form>
                            
                        </div>
                        </div>
                    </div>

                </div>
            </div>
<?php include 'includes/footer.php'; ?>
<?php include 'includes/header.php'; ?>
                <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col accordion-head">
                        <div class="accordion-head-title">Top Up</div>
                        <div class="accordions">

                            <div class="accordion">
                                <div class="accordion-title">How do I Top Up my account?</div>
                                <div class="accordion-content">We have 2 Methods of topping up your Account:
                                    <ol>
                                        <li>Top Up by Card: 
                                            You can use this this method to top up your Account directly from your Bank Card. Simply enter the Amount you wish to Top Up and Click to Deposit By card. You will be prompted to enter your card details on the Payment Page and the transaction is immediately processed. Transactions through Cards are always 3D Secured.
                                        </li>
                                        <li> Top Up By Bank: 
                                        Your Virtual Bank Account Details such as Beneficiary Name, IBAN and BIC are shown on:
                                        <ul>
                                            <li style="list-style-type:circle">The Top Up by Bank Transfer Section.</li>
                                            <li style="list-style-type:circle">By Clicking the Box in the top right corner of the browser.</li>
                                        </ul>
                                    You can simply input that information on your Online Banking to execute a SEPA transfer to Top Up your Account, directly from your Bank Account.
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="accordion">
                                <div class="accordion-title">How do I Top Up my account?</div>
                                <div class="accordion-content">We have 2 Methods of topping up your Account:
                                    <ol>
                                        <li>Top Up by Card: 
                                            You can use this this method to top up your Account directly from your Bank Card. Simply enter the Amount you wish to Top Up and Click to Deposit By card. You will be prompted to enter your card details on the Payment Page and the transaction is immediately processed. Transactions through Cards are always 3D Secured.
                                        </li>
                                        <li> Top Up By Bank: 
                                        Your Virtual Bank Account Details such as Beneficiary Name, IBAN and BIC are shown on:
                                        <ul>
                                            <li style="list-style-type:circle">The Top Up by Bank Transfer Section.</li>
                                            <li style="list-style-type:circle">By Clicking the Box in the top right corner of the browser.</li>
                                        </ul>
                                    You can simply input that information on your Online Banking to execute a SEPA transfer to Top Up your Account, directly from your Bank Account.
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col accordion-head">
                        <div class="accordion-head-title">Top Up</div>
                        <div class="accordions">

                            <div class="accordion">
                                <div class="accordion-title">How do I Top Up my account?</div>
                                <div class="accordion-content">We have 2 Methods of topping up your Account:
                                    <ol>
                                        <li>Top Up by Card: 
                                            You can use this this method to top up your Account directly from your Bank Card. Simply enter the Amount you wish to Top Up and Click to Deposit By card. You will be prompted to enter your card details on the Payment Page and the transaction is immediately processed. Transactions through Cards are always 3D Secured.
                                        </li>
                                        <li> Top Up By Bank: 
                                        Your Virtual Bank Account Details such as Beneficiary Name, IBAN and BIC are shown on:
                                        <ul>
                                            <li style="list-style-type:circle">The Top Up by Bank Transfer Section.</li>
                                            <li style="list-style-type:circle">By Clicking the Box in the top right corner of the browser.</li>
                                        </ul>
                                    You can simply input that information on your Online Banking to execute a SEPA transfer to Top Up your Account, directly from your Bank Account.
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="accordion">
                                <div class="accordion-title">How do I Top Up my account?</div>
                                <div class="accordion-content">We have 2 Methods of topping up your Account:
                                    <ol>
                                        <li>Top Up by Card: 
                                            You can use this this method to top up your Account directly from your Bank Card. Simply enter the Amount you wish to Top Up and Click to Deposit By card. You will be prompted to enter your card details on the Payment Page and the transaction is immediately processed. Transactions through Cards are always 3D Secured.
                                        </li>
                                        <li> Top Up By Bank: 
                                        Your Virtual Bank Account Details such as Beneficiary Name, IBAN and BIC are shown on:
                                        <ul>
                                            <li style="list-style-type:circle">The Top Up by Bank Transfer Section.</li>
                                            <li style="list-style-type:circle">By Clicking the Box in the top right corner of the browser.</li>
                                        </ul>
                                    You can simply input that information on your Online Banking to execute a SEPA transfer to Top Up your Account, directly from your Bank Account.
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                </div>
<?php include 'includes/footer.php'; ?>
<?php include 'includes/header.php'; ?>

            <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col block">
                        <div class="mdl-tabs mdl-js-tabs">
                        <div class="mdl-tabs__tab-bar">
                            <a href="#tab1" class="mdl-tabs__tab is-active"><i class="fas fa-university"></i> Load From Credit Card</a>
                            <a href="#tab2" class="mdl-tabs__tab"><i class="fas fa-credit-card"></i> Load From Your Iban</a>
                        </div>
                        <div class="mdl-tabs__panel is-active" id="tab1">
                            
                            <form action="#">
                                <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--6-col">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                            <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                            <input type="hidden" value="" name="type">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            <label for="type" class="mdl-textfield__label">Currency</label>
                                            <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <li class="mdl-menu__item" data-val="EUR">EUR</li>
                                                <li class="mdl-menu__item" data-val="GBP">GBP</li>
                                                <li class="mdl-menu__item" data-val="USD">USD</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mdl-cell mdl-cell--6-col">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                            <input class="mdl-textfield__input" type="text" id="amount">
                                            <label class="mdl-textfield__label" for="amount">Amount to Load</label>
                                        </div>
                                    </div>
                                    <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore float-right">
                                </div>
                            </form>

                        </div>
                        <div class="mdl-tabs__panel" id="tab2">
                            
                        <form action="#">
                            <div class="mdl-grid">
                            <div class="mdl-cell mdl-cell--9-col">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="Currency" value="IE57PFSR99107000661733" readonly>
                                        <label class="mdl-textfield__label" for="Currency">From My Iban</label>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--3-col">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="Currency" value="EUR" readonly>
                                        <label class="mdl-textfield__label" for="Currency">Currency</label>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--9-col">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                        <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                        <input type="hidden" value="" name="type">
                                        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                        <label for="type" class="mdl-textfield__label">To Card</label>
                                        <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                            <li class="mdl-menu__item" data-val="GBP">8762 14** **** 6020</li>
                                            <li class="mdl-menu__item" data-val="USD">8762 14** **** 6021</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--3-col">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="amount">
                                        <label class="mdl-textfield__label" for="amount">Amount to Load</label>
                                    </div>
                                </div>
                                <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore float-right">
                            </div>
                            </form>

                        </div>
                        </div>
                    </div>

                </div>
            </div>
<?php include 'includes/footer.php'; ?>
<?php include 'includes/header.php'; ?>

            <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col account_balances_dash">
                        <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account-balance-title">Account Summary</div>

                                <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>iBan No</h4>
                                    <p>IE57PFSR99107000661733</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Currency</h4>
                                    <p>EUR</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Balance</h4>
                                    <p class="balance">12,232,333<span>.00</span></p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>View</h4>
                                    <p>Account Details</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Action</h4>
                                    <p><a href="deposit.php">Load Iban</a></p>
                                </div>

                                <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Card No</h4>
                                    <p>876214******6020</p>
                                    <p>876214******6021</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Card ID</h4>
                                    <p>876214******602</p>
                                    <p>876214******602</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Currencies</h4>
                                    <p>EUR, GBP, USD</p>
                                    <p>EUR, GBP, USD</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>View</h4>
                                    <p>Card Details</p>
                                    <p>Card Details</p>
                                </div>
                                <div class="mdl-cell mdl-cell--2-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account--total">
                                    <h4>Action</h4>
                                    <p><a href="deposit.php">Load Card</a></p>
                                    <p><a href="deposit.php">Load Card</a></p>
                                </div>
                        </div>
                    </div>

                    <a href="#" class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--4-col-tablet block shortcut">
                        <i class="far fa-credit-card fa-3x"></i>
                        <div class="block--title">Order New Card</div>
                    </a>
                    <a href="#" class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--4-col-tablet block shortcut">
                        <i class="far fa-credit-card fa-3x"></i>
                        <div class="block--title">Account Statement</div>
                    </a>
                    <a href="/deposit2.php" class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--4-col-tablet block shortcut">
                        <i class="far fa-money-bill-alt fa-3x"></i>
                        <div class="block--title">Load from Credit Card</div>
                    </a>
                    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--4-col-tablet block shortcut red--icon">
                        <i class="fas fa-ban fa-3x"></i>
                        <div class="block--title">Unverified Account</div>
                    </div>
                    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--4-col-tablet block shortcut green--icon">
                        <i class="fas fa-check fa-3x"></i>
                        <div class="block--title">Verified Account</div>
                    </div>

                    <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--6-col-desktop block shortcut">
                        <i class="fas fa-life-ring fa-3x"></i>
                        <div class="block--title">If you have any query, search through our <a href="#">FAQ</a>'s or <a href="#" class="drawer-link">Submit a Ticket</a> Or call us @ <a href="#">+44 00 00000000</a></div>
                    </div>

                    <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--6-col-desktop block shortcut">
                        <i class="fas fa-newspaper fa-2x"></i>
                        <div class="block--title">Ford to ramp up Lincoln rollout in China in bid to catch rivals: sources</div>
                        <a href="#">Read More <i class="fas fa-angle-right"></i></a>
                    </div>

                </div>
            </div>

<?php include 'includes/footer.php'; ?>
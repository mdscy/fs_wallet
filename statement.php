<?php include 'includes/header.php'; ?>
            <div class="page--content">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--12-col block">

                        <form action="#">
                        <div class="mdl-grid">
                        
                            <div class="mdl-cell mdl-cell--4-col">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input mdl-datepicker__input" type="text" id="startdate">
                                    <label class="mdl-textfield__label" for="startdate">Start Date</label>
                                </div>
                            </div>
                            <div class="mdl-cell mdl-cell--4-col">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input mdl-datepicker__input" type="text" id="enddate">
                                    <label class="mdl-textfield__label" for="enddate">End Date</label>
                                </div>
                            </div>
                            <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
                        
                        </div>
                        </form>
                    </div>
                </div>
            </div>
<?php include 'includes/footer.php'; ?>
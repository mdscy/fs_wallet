<?php include 'includes/header.php'; ?>

            <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col block">
                        <div class="mdl-tabs mdl-js-tabs">
                        <div class="mdl-tabs__tab-bar">
                            <a href="#tab1" class="mdl-tabs__tab is-active"><i class="fas fa-file-alt"></i> Upload Documents</a>
                            <a href="#tab2" class="mdl-tabs__tab"><i class="fas fa-clipboard-check"></i> Limits</a>
                        </div>
                        <div class="mdl-tabs__panel is-active" id="tab1">
                            <div class="mdl-grid">

                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                    <h4>Upload Documents</h4>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select countrycode">
                                    <input type="text" value="" class="mdl-textfield__input" id="cardchangetatus" readonly>
                                    <input type="hidden" value="" name="cardchangetatus">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                    <label for="cardchangetatus" class="mdl-textfield__label">Choose Identification Document</label>
                                    <ul for="cardchangetatus" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                        <li class="mdl-menu__item" data-val="open">Passport</li>
                                        <li class="mdl-menu__item" data-val="lost">Nationality ID</li>
                                    </ul>
                                </div>
                        </div>

                                <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet mdl-cell--12-mobile">
                                    <p><b>Passport Cover</b><br/>Color copy of the Cover of your Passport. Accepted Files : "jpg", "jpeg","png", "gif","pdf", Maximum File Size : less than 4MB</p>
                                    <form action="" method="post" enctype="multipart/form-data" class="dropzone">
                                    <input type="file" name="file" multiple/>
                                    <span>
                                        <i class="fas fa-cloud-upload-alt fa-4x"></i>
                                        <p>Upload your documents here</p>
                                    </span>
                                    </form>
                                </div>
                                <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet mdl-cell--12-mobile">
                                    <p><b>Passport Cover</b><br/>Color copy of the Cover of your Passport. Accepted Files : "jpg", "jpeg","png", "gif","pdf", Maximum File Size : less than 4MB</p>
                                    <form action="" method="post" enctype="multipart/form-data" class="dropzone">
                                    <input type="file" name="file" multiple/>
                                    <span>
                                        <i class="fas fa-cloud-upload-alt fa-4x"></i>
                                        <p>Upload your documents here</p>
                                    </span>
                                    </form>
                                </div>
                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet mdl-cell--12-mobile text-align-right">
                                    <input type="submit" value="Submit Passport" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
                                </div>

                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                    <p><b>Address documents</b><br/> (Please upload a color copy of your Proof of Address)</p>
                                    <p>The Proof Of Address needs to be dated within the last 3 months. The full name and address added to the onboarding form must match the information on the Proof Of Address.</p>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                    <form action="" method="post" enctype="multipart/form-data" class="dropzone">
                                    <input type="file" name="file" multiple/>
                                    <span>
                                        <i class="fas fa-cloud-upload-alt fa-4x"></i>
                                        <p>Upload your documents here</p>
                                    </span>
                                    </form>
                                </div>
                                <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet mdl-cell--12-mobile text-align-right">
                                    <input type="submit" value="Submit Proof of Address" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
                                </div>


                            </div>
                        </div>
                        <div class="mdl-tabs__panel" id="tab2">
                            
                            
                        </div>
                        </div>
                    </div>

                </div>
            </div>
<?php include 'includes/footer.php'; ?>
    <footer>
        <div class="footer--global">
            <div class="page--content">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet">© <?php echo date("Y"); ?> FS Technologies Limited. All rights reserved.</div>
                    <div class="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet footer--links">
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#" class="drawer-link">Submit a Ticket</a></li>
                        <li><a href="#">Call Support</a></li>
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet text-align-center mastecardlogo"><img src="img/ojf8ed4taaxccncp6pcp.png"> <img src="img/mc_vrt_pos.png"></div>
                    <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet text-align-center copyright">THE FS TECHNOLOGIES PREPAID MASTERCARD IS ISSUED BY PREPAID FINANCIAL SERVICES LIMITED PURSUANT TO A LICENSE FROM MASTERCARD INTERNATIONAL INCORPORATED. PREPAID FINANCIAL SERVICES LIMITED IS REGULATED AND AUTHORISED BY THE FINANCIAL CONDUCT AUTHORITY, AS AN ELECTRONIC MONEY INSTITUTION, REGISTRATION NUMBER 900036. REGISTERED OFFICE: FIFTH FLOOR, LANGHAM HOUSE, 302-308 REGENT STREET, LONDON, W1B 3AT. COMPANY REGISTRATION NUMBER: 06337638.</div>
                </div>
            </div>
    </footer>

    <div class="drawer">
        <div class="drawer__content">
            <h3>Submit a Ticket</h3>
            <h5>Are you facing any issues? Fill in this form and we will get back to you within 24 hours.</h5>
            <form action="#">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                    <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                    <input type="hidden" value="" name="type">
                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                    <label for="type" class="mdl-textfield__label">Concern Type</label>
                    <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                        <li class="mdl-menu__item" data-val="Account Related">Account Related</li>
                        <li class="mdl-menu__item" data-val="Card Related">Card Related</li>
                        <li class="mdl-menu__item" data-val="Other Issue">Other Issue</li>
                    </ul>
                </div>
            </div>
                <div class="mdl-textfield mdl-js-textfield">
                    <textarea class="mdl-textfield__input" type="text" rows= "3" id="sample5" ></textarea>
                    <label class="mdl-textfield__label" for="sample5">Message</label>
                </div>
                <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
            </form>

            <span class="icon icon--close"><i class="material-icons">close</i></span>
        </div>
    </div>
    <div class="overlay"></div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/app.js?v=120620181223"></script>
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/material.min.css">
        <link rel="stylesheet" href="css/style.css?v=120620181223">
        <script src="js/material.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
        <title>Header</title>
    </head>
    <body>
    <div class="menu-link button-mob">
        <i class="material-icons">menu</i>
    </div>
    <div class="mdl-layout mdl-js-layout header">

    <div class="menu-mob">
        <div class="menu__content">
            <a href="dashboard.php" class="active"><i class="fas fa-th-large"></i> Dashboard</a>
            <div class="loadfunds-menu2">   
                <button id="loadfunds-menu2" class="mdl-button mdl-js-button">
                    <i class="far fa-credit-card"></i> Load Funds
                </button>

                <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="loadfunds-menu2">
                    <li class="mdl-menu__item"><a href="deposit.php">Load My IBAN</a></li>
                    <li class="mdl-menu__item"><a href="deposit2.php">Load My Card</a></li>
                </ul>
            </div>
            <a href="transfer.php"><i class="fas fa-exchange-alt"></i> Transfer Funds</a>
            <a href="accounts.php"><i class="far fa-clone"></i> Accounts</a>
            <a href="statement.php"><i class="far fa-file-alt"></i> Statements</a>
            <a href="faq.php"><i class="fas fa-life-ring"></i> Support</a>
            <div class="user-menu">   
                <button id="usermenu" class="mdl-button mdl-js-button">
                    <i class="far fa-user"></i> My Account
                </button>

                <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="usermenu">
                    <li class="mdl-menu__item"><a href="accountdetails.php">Account Details</a></li>
                    <li class="mdl-menu__item"><a href="cardstatus.php">Change Card Status</a></li>
                    <li class="mdl-menu__item"><a href="verification.php">Verification and Limits</a></li>
                </ul>
            </div>

            <span class="icon icon--close-menu"><i class="material-icons">close</i></span>
        </div>
    </div>
    <div class="overlay-for-menu"></div>
        <header class="header--global">
            <div class="page--content">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--6-col mdl-cell--12-col-phone mdl-cell--4-col-tablet logo"><img src="img/fs--logo.png"></div>
                    <div class="mdl-cell mdl-cell--6-col mdl-cell--12-col-phone mdl-cell--4-col-tablet">
                        <span class="mdl-list__item-primary-content float-right mdl-cell--hide-phone">
                            <!-- User Photo. Get it from Gravatar or allowed user to upload it -->
                            <div class="mdl-list__item-avatar"></div>
                        </span>
                        <div class="user--details">
                            <div class="fullname">Name Surname</div>
                            <div class="wallet_id">Wallet ID: <b>400000243466</b></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="mdl-cell--hide-tablet mdl-cell--hide-phone">
            <div class="page--content">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--12-col">
                        <a href="dashboard.php" class="active"><i class="fas fa-th-large"></i> Dashboard</a>
                        <div class="loadfunds-menu">   
                            <button id="loadfunds-menu" class="mdl-button mdl-js-button">
                                <i class="far fa-credit-card"></i> Load Funds
                            </button>

                            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="loadfunds-menu">
                                <li class="mdl-menu__item"><a href="deposit.php">Load My IBAN</a></li>
                                <li class="mdl-menu__item"><a href="deposit2.php">Load My Card</a></li>
                            </ul>
                        </div>
                        <a href="transfer.php"><i class="fas fa-exchange-alt"></i> Transfer Funds</a>
                        <a href="accounts.php"><i class="far fa-clone"></i> Accounts</a>
                        <a href="statement.php"><i class="far fa-file-alt"></i> Statements</a>
                        <a href="faq.php"><i class="fas fa-life-ring"></i> Support</a>
                        <div class="user-menu">
                            
                            <button id="user-menu-lower" class="mdl-button mdl-js-button">
                                <i class="far fa-user"></i> My Account
                            </button>

                            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="user-menu-lower">
                                <li class="mdl-menu__item"><a href="accountdetails.php">Account Details</a></li>
                                <li class="mdl-menu__item"><a href="cardstatus.php">Change Card Status</a></li>
                                <li class="mdl-menu__item"><a href="verification.php">Verification and Limits</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
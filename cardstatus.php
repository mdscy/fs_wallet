<?php include 'includes/header.php'; ?>
            <div class="page--content">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--12-col block">

                    <form action="#">
                            <div class="mdl-grid">
                            
                            <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                <h4>Change Card Status</h4>
                            </div>
                            <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select countrycode">
                                    <input type="text" value="" class="mdl-textfield__input" id="card" readonly>
                                    <input type="hidden" value="" name="card">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                    <label for="card" class="mdl-textfield__label">Choose Card</label>
                                    <ul for="card" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                        <li class="mdl-menu__item" data-val="EUR">54564946464564556456465</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="cardactivestatus" value="Issued" readonly>
                                    <label class="mdl-textfield__label" for="cardactivestatus">Current Status</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select countrycode">
                                    <input type="text" value="" class="mdl-textfield__input" id="cardchangetatus" readonly>
                                    <input type="hidden" value="" name="cardchangetatus">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                    <label for="cardchangetatus" class="mdl-textfield__label">New Status</label>
                                    <ul for="cardchangetatus" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                        <li class="mdl-menu__item" data-val="open">Open</li>
                                        <li class="mdl-menu__item" data-val="lost">Lost</li>
                                        <li class="mdl-menu__item" data-val="stolen">Stolen</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <input type="submit" value="Submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore" data-upgraded=",MaterialButton">
                            </div>

                    </form>
                    </div>
                </div>
            </div>
<?php include 'includes/footer.php'; ?>
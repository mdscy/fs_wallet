<?php include 'includes/header.php'; ?>

            <div class="page--content">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--12-col account_balances_dash">
                        <div class="mdl-grid">
                            <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet account-balance-title">Account Summary</div>
                            <div class="mdl-cell mdl-cell--4-col mdl-cell--12-col-phone mdl-cell--4-col-tablet account--total">
                                €<h2>12,349,124</h2>.00
                                <p><b>EUR</b> Balance</p>
                            </div>
                            <div class="mdl-cell mdl-cell--4-col mdl-cell--12-col-phone mdl-cell--4-col-tablet account--total">
                                $<h2>122,000</h2>.00
                                <p><b>USD</b> Balance</p>
                            </div>
                            <div class="mdl-cell mdl-cell--4-col mdl-cell--12-col-phone mdl-cell--4-col-tablet account--total">
                                £<h2>141,000</h2>.00
                                <p><b>GBP</b> Balance</p>
                            </div>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet add-currency">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                <input type="text" value="" class="mdl-textfield__input" id="addcurrency">
                                <input type="hidden" value="" name="addcurrency">
                                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                <label for="addcurrency" class="mdl-textfield__label">Currency</label>
                                <ul for="addcurrency" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                    <li class="mdl-menu__item" data-val="USD">USD</li>
                                    <li class="mdl-menu__item" data-val="GBP">GBP</li>
                                </ul>
                            </div>
                            <button class="mdl-button mdl-button--raised mdl-js-button dialog-button"><i class="fas fa-plus-square"></i> Add Currency</button>
                        </div>
                    </div>

                </div>
            </div>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/header.php'; ?>
            <div class="page--content">
                <div class="mdl-grid">
                <form action="#">

                        <div class="mdl-cell mdl-cell--12-col block">

                            <div class="mdl-grid">
                            
                            <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                <h4>1. General Info</h4>
                            </div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="firstname" value="First Name" readonly>
                                    <label class="mdl-textfield__label" for="firstname">First Name</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="lastname" value="Last Name" readonly>
                                    <label class="mdl-textfield__label" for="lastname">Last Name</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="email" value="Email" readonly>
                                    <label class="mdl-textfield__label" for="email">Email</label>
                                </div>
                            </div>     
                            
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="birthday" value="68/41/3168345" readonly>
                                    <label class="mdl-textfield__label" for="birthday">Email</label>
                                </div>
                            </div>   

                            <div class="mdl-cell mdl-cell--2-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select countrycode">
                                    <input type="text" value="" class="mdl-textfield__input" id="type" readonly>
                                    <input type="hidden" value="" name="type">
                                    <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                    <label for="type" class="mdl-textfield__label">Code</label>
                                    <ul for="type" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                        <li class="mdl-menu__item" data-val="EUR">+30</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--4-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="phonenumber" value="3168345" readonly>
                                    <label class="mdl-textfield__label" for="phonenumber">Phone</label>
                                </div>
                            </div>   

                            <div class="mdl-cell mdl-cell--4-col mdl-cell--12-tablet">
                                    Gender
                                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                                    <input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1">
                                    <span class="mdl-radio__label">Female</span>
                                    </label>
                                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                    <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="2">
                                    <span class="mdl-radio__label">Male</span>
                                    </label>
                            </div>
                            <div class="mdl-cell mdl-cell--12-col mdl-cell--12-tablet">
                                <h4>2. Address Details</h4>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="addressline1">
                                    <label class="mdl-textfield__label" for="addressline1">Address Line 1</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="addressline2">
                                    <label class="mdl-textfield__label" for="addressline2">Address Line 2</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="postcode">
                                    <label class="mdl-textfield__label" for="postcode">Post Code</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="city">
                                    <label class="mdl-textfield__label" for="city">Limassol</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="country">
                                    <label class="mdl-textfield__label" for="country">Country</label>
                                </div>
                            </div>

                            <div class="mdl-cell mdl-cell--6-col mdl-cell--12-tablet">
                                <input type="submit" value="Add New Address" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore" data-upgraded=",MaterialButton">
                            </div>




                            <div class="mdl-components__warning">
                                    <p><b>Important Note:</b></p>
                                    <p>1. Please notify us on any changes of your above account details so we can update your account. It is your obligation to keep your account details up-to-date and valid.</p>
                            </div>

                        </div>

                    </div>
                </form>
                </div>
            </div>
<?php include 'includes/footer.php'; ?>